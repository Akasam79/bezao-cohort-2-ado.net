﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using BEZAOPayDAL.Models;

namespace BEZAOPayDAL
{
   public class BEZAODAL
   {
       private readonly string _connectionString;

       public BEZAODAL():
           this(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=BEZAOPay;Integrated Security=True")
       {

       }
       
       public BEZAODAL(string connectionString )
       {
           _connectionString = connectionString;
       }


       private SqlConnection _sqlConnection = null;
       private void OpenConnection()
       {
           _sqlConnection = new SqlConnection { ConnectionString = _connectionString };
           _sqlConnection.Open();
       }

       private void CloseConnection()
       {
           if (_sqlConnection?.State != ConnectionState.Closed) 
               _sqlConnection?.Close();
       }


       public IEnumerable<User> GetAllUsers()
       {
            OpenConnection();

            var users = new List<User>();

            var query = @"SELECT * FROM USERS";

            using (var command = new SqlCommand(query, _sqlConnection))
            {
                command.CommandType = CommandType.Text;
                var reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                while (reader.Read())
                {
                   users.Add(new User
                   {
                       Id = (int) reader["Id"],
                       Name = (string) reader["Name"],
                       Email =  (string) reader["Email"]


                   }); 
                }
                reader.Close();
            }

            return users;

       }

        public User GetUserByID(int id)
        {
            OpenConnection();
            User aUser = null;
            string sql = $"Select * From USERS where id = {id}";
            try{
                using (SqlCommand command = new SqlCommand(sql, _sqlConnection))
                {
                    command.CommandType = CommandType.Text;
                    SqlDataReader dataReader = command.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dataReader.Read())
                    {
                        aUser = new User
                        {
                            Id = (int)dataReader["Id"],
                            Name = (string)dataReader["Name"],
                            Email = (string)dataReader["Email"]
                        };


                    }
                    dataReader.Close();
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("An error occured " + ex.Message);
            }
            return aUser;
        }

        public void Insert(string name, string email)
        {
            OpenConnection();

            string sql = $"Insert Into USERS (Name, Email) Values ('{name}', '{email}')";

             using (SqlCommand command = new SqlCommand(sql, _sqlConnection))
                {
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                }
                CloseConnection();
        }

        public void DeleteUserById(int id)
        {
            OpenConnection();

            string sql = $"Delete from USERS where Id = '{id}'";

            using (SqlCommand command = new SqlCommand(sql, _sqlConnection))
            {
                try
                {
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                }
                catch (SqlException ex )
                {
                    Exception error = new Exception("Sorry! Can't delete this detail!", ex);
                    throw error;
                }
            }
            CloseConnection();
        }

        public void UpdateUserName(int id, string newName)
        {
            OpenConnection();
            string sql = $"Update USERS Set Name = '{newName}' Where Id = '{id}'";

            if (string.IsNullOrWhiteSpace(newName))
            {
                Console.WriteLine("Name cannot be empty or whitespace in name update");
            }

            using (SqlCommand command = new SqlCommand(sql, _sqlConnection))
            {
                command.ExecuteNonQuery();
            }
            CloseConnection();
        }


        public void CreateUserByStoredProcedure(string Name, string Email)
        {
            OpenConnection();

            using (SqlCommand command = new SqlCommand("createUser", _sqlConnection))
            {
                command.CommandType = CommandType.StoredProcedure;
                
                command.Parameters.AddWithValue("@Name", Name);
                command.Parameters.AddWithValue("@Email", Email);
                command.ExecuteNonQuery();
            }
        }

        public User GetNameBystoredProcedure(int Id)
        {
            OpenConnection();
            User aUser = null;
            try
            {
                using (SqlCommand command = new SqlCommand("GetName", _sqlConnection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Id", Id);

                    SqlDataReader dataReader = command.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dataReader.Read())
                    {
                        aUser = new User
                        {
                            Id = (int)dataReader["Id"],
                            Name = (string)dataReader["Name"],
                            Email = (string)dataReader["Email"]
                        };


                    }
                    dataReader.Close();

                }
                
            }
            catch(Exception ex)
            {
                Console.WriteLine("An error occured here" + ex.Message);
            }
            return aUser;
        }

        public void Transfer(Account account, Transaction transact)
        {
            OpenConnection();
            string sql = $"Update Accounts Set Balance = '{account.Balance + account.Balance}' Where Id = '{account.Id}'";
            string query = "Insert Into Transactions (UserId, Mode, Amount, Time) Values " +
                $"('{transact.UserId}', '{transact.Mode}', '{transact.Amount}', '{transact.Time}')";

            var cmdSql = new SqlCommand(sql, _sqlConnection);
            var cmdQuery = new SqlCommand(query, _sqlConnection);

            SqlTransaction tx = null;

            try
            {
                tx = _sqlConnection.BeginTransaction();
                cmdSql.Transaction = tx;
                cmdQuery.Transaction = tx;

                cmdSql.ExecuteNonQuery();
                cmdQuery.ExecuteNonQuery();

                tx.Commit();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                tx?.Rollback();
            }
            finally
            {
                CloseConnection();
            }
        }



    }
}
//SqlParameter parameter = new SqlParameter
//{
//    ParameterName = @Name,
//    SqlDbType = SqlDbType.NVarChar,
//    Value = Name,
//    Size = 50,

//};
//command.Parameters.Add(parameter);

//parameter = new SqlParameter
//{
//    ParameterName = @Email,
//    SqlDbType = SqlDbType.NVarChar,
//    Value = Email,
//    Size = 50,
//};