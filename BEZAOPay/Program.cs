﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEZAOPayDAL;


namespace BEZAOPay
{
    class Program
    {
        static void Main(string[] args)
        {
            var db = new BEZAODAL();

            //*comment Insert
            //db.Insert("Chukwuebuka2", "ebuka2@gmail.com");

            //*comment UpdateAUser

            //db.UpdateUserName(17, "NewEbuka");

            //*comment calling the storedProcedure to add a new user Samuel
            //db.CreateUserByStoredProcedure("@Name = Samuel", "sam@gmail.com");

            var users = db.GetAllUsers();
            //Get all User
            foreach (var user in users)
            {
                Console.WriteLine($"Id: {user.Id}\nName: {user.Name}\nEmail: {user.Email}");
            }

            var newResult = db.GetNameBystoredProcedure(16);
            Console.WriteLine($"Id: {newResult.Id}\nName: {newResult.Name}\nEmail: {newResult.Email}");

            //Implementing Transactions
            //db.Transfer(new BEZAOPayDAL.Models.Account {UserId = 10, Balance = 1000000.70m },
            //    new BEZAOPayDAL.Models.Transaction {UserId = 10, Mode = "Credit", Amount = 1000000.88m, Time = DateTime.Now });

            //DeleteUserById
            //db.DeleteUserById(17);


            //GetUserById
            var aUser = db.GetUserByID(12);
            Console.WriteLine("\t Id : {0}\n \t Name: {1} \n \t Email: {2}", aUser.Id, aUser.Name, aUser.Email);


        }
    }
}
